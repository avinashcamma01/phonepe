package com.example.phonpeproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.example.phonpeproject.view.LogoAdapter
import com.example.phonpeproject.viewModel.LogoViewModel

class MainActivity : AppCompatActivity() {
    lateinit var logoViewModel : LogoViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pager = findViewById<ViewPager2>(R.id.viewpager_logos)
        logoViewModel = ViewModelProvider(this).get(LogoViewModel::class.java)
        logoViewModel.logoList.observe(this, Observer {logoList->
            Log.d("Logo List",logoList.toString())
            pager?.adapter =this?.let { LogoAdapter(this, logoList) }
            pager?.orientation = ViewPager2.ORIENTATION_VERTICAL
        })
    }
}