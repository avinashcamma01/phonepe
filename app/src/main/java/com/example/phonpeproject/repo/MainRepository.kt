package com.example.phonpeproject.repo

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.phonpeproject.model.Logo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException

class MainRepository(var context: Context) {
    private var mutableLiveData = MutableLiveData<List<Logo>>()
    fun getLogoList(): MutableLiveData<List<Logo>>{
        val jsonFileString = getJsonDataFromAsset(context, "logo.txt")
        val gson = Gson()
        val logoList = object : TypeToken<List<Logo>>() {}.type
        var logo: List<Logo> = gson.fromJson(jsonFileString, logoList)
        mutableLiveData.value = logo
        return mutableLiveData
    }

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}