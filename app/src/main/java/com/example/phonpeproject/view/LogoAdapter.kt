package com.example.phonpeproject.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.broooapps.otpedittext2.OtpEditText
import com.bumptech.glide.Glide
import com.example.phonpeproject.R
import com.example.phonpeproject.model.Logo

class LogoAdapter(private val context: Context, private val logo: List<Logo>): RecyclerView.Adapter<LogoAdapter.PageHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PageHolder  =
        PageHolder(LayoutInflater.from(context).inflate(R.layout.logo_item_view, parent, false))

    override fun onBindViewHolder(holder: PageHolder, position: Int) {
        Glide.with(context).load(logo[position].imgUrl).into(holder.logoImgView)
        holder.textView.setText(logo[position].name)
    }

    override fun getItemCount(): Int = logo.size

    inner class PageHolder(view: View): RecyclerView.ViewHolder(view){
        val textView = view.findViewById<TextView>(R.id.txtView_logoname)
        val logoImgView = view.findViewById<ImageView>(R.id.iv_logo)
    }

}