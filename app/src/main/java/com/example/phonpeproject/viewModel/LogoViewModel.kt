package com.example.phonpeproject.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.phonpeproject.model.Logo
import com.example.phonpeproject.repo.MainRepository

class LogoViewModel (application:Application)  : AndroidViewModel(application) {
    val repository = MainRepository(application)
    val logoList : MutableLiveData<List<Logo>> get() = repository.getLogoList()
}